﻿using ConfigurationData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTestT4
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Prueba codigo..");

            JObject dataJson = JObject.Parse(File.ReadAllText(@"C:\Users\camilo.ortiz\source\repos\CodeTestT4\CodeTestT4\DataCodeTextT4.json"));

            Console.WriteLine(dataJson["Models"]);

            Example ob = new Example();
            List<ModelClass> model = ob.TestData();

            Console.WriteLine(JsonConvert.SerializeObject(model));

            foreach (var item in model)
            {
                string projectPath = @"C:\Users\camilo.ortiz\source\repos\CodeTestT4\CodeTestT4\";

                string folderName = Path.Combine(projectPath, item.Scheme);
                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }

                Console.WriteLine(item.ModelName);
                Console.WriteLine(item.Scheme);

                List<Fields> field = JsonConvert.DeserializeObject<List<Fields>>(item.Fields);

                Console.WriteLine(field.Count);
            }

            Console.ReadLine();
        }
    }
}
