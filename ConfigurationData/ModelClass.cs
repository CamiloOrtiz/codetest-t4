﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationData
{
    public class ModelClass
    {
        public string ModelName { get; set; }
        public string Scheme { get; set; }
        public string Fields { get; set; }
    }
}
