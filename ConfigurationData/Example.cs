﻿using AutoMapper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationData
{
    public class Example
    {

        public List<ModelClass> TestData()
        {

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<JObject, ModelClass>()
                .ForMember("ModelName", field => { field.MapFrom(jo => jo["ModelName"]); })
                .ForMember("Scheme", field => { field.MapFrom(jo => jo["Scheme"]); })
                .ForMember("Fields", field => { field.MapFrom(jo => jo["Fields"]); });

                //cfg.CreateMap<JObject, Fields>()
                //.ForMember("AsociatedDecorator", field => { field.MapFrom(jo => jo["AsociatedDecorator"]); })
                //.ForMember("DataType", field => { field.MapFrom(jo => jo["DataType"]); })
                //.ForMember("FieldName", field => { field.MapFrom(jo => jo["FieldName"]); });
            });

            IMapper mapper = config.CreateMapper();

            JObject dataJson = JObject.Parse(File.ReadAllText(@"C:\Users\camilo.ortiz\source\repos\CodeTestT4\CodeTestT4\DataCodeTextT4.json"));

            List<ModelClass> model = mapper.Map<List<ModelClass>>(dataJson["Models"]);

            return model;

        }
    }
}
