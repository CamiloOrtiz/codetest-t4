﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigurationData
{
    public class Fields
    {

        public string AsociatedDecorator { get; set; }
        public string DataType { get; set; }
        public string FieldName { get; set; }

    }
}
